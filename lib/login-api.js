const axios = require('axios')
const querystring = require('querystring')
const { URLSearchParams } = require('url')
const lineOAuth = require('./lineOAuth')('LOGIN')

const clientId = process.env.LOGIN_CHANNEL_ID
const clientSecret = process.env.LOGIN_CHANNEL_SECRET

const baseUrl = 'https://api.line.me'
const endpoint = `${baseUrl}/oauth2/v2.1`

const middleware = lineOAuth.authorize({ clientId, clientSecret })

const verifyToekn = (token) => {
  return axios.get(`${endpoint}/revoke?access_token=${token}`)
}

const refreshToken = (refresh_token) =>{
  const params = {
    client_id: config.clientId,
    client_secret: config.clientSecret,
    refresh_token,
    grant_type: 'refresh_token'
  }
  return axios.post(`${endpoint}/token`, querystring.stringify(params))
}

const revokeToken = (token) => {
  const params = new URLSearchParams({
    access_token:token,
    client_id: clientId,
    client_secret: clientSecret
  })
  return axios.post(`${endpoint}/revoke`, querystring.stringify(params), {headers: {Authorization: `Bearer ${token}`}})
}

const getProfile = (token) => {
  return axios.get(`${baseUrl}/v2/profile`, {headers: {Authorization: `Bearer ${token}`}})
}

const getFriendshipStatus = (token) => {
  return axios.get(`${baseUrl}/friendship/v1/status`, {headers: {Authorization: `Bearer ${token}`}})
}

module.exports = lineOAuth
module.exports.expressMiddleware = middleware
module.exports.verifyToekn = verifyToekn
module.exports.refreshToken = refreshToken
module.exports.revokeToken = revokeToken
module.exports.getProfile = getProfile
module.exports.getFriendshipStatus = getFriendshipStatus
