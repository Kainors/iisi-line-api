const linebot = require('linebot')

const channelId = process.env.BOT_CHANNEL_ID
const channelSecret = process.env.BOT_CHANNEL_SECRET
const channelAccessToken = process.env.BOT_CHANNEL_ACCESS_TOKEN

const bot = linebot({ channelId, channelSecret, channelAccessToken });

module.exports = bot
