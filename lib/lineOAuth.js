/**
 * Modify from express-line-notify
 * https://github.com/HoMuChen/express-line-notify
 */
const url = require('url');
const { URLSearchParams } = url;
const https = require('https');
const querystring = require('querystring');
const axios = require('axios');
const jwtDecode = require('jwt-decode')

const LOGIN_API_AUTH = 'https://access.line.me/oauth2/v2.1/authorize'
const LOGIN_API_TOKEN ='https://api.line.me/oauth2/v2.1/token'

const NOTIFY_API_AHTH ='https://notify-bot.line.me/oauth/authorize'
const NOTIFY_API_TOKEN ='https://notify-bot.line.me/oauth/token'

class LineOAuth {
  constructor(type) {
    this.type = type
    switch (type) {
      case 'LOGIN':
        this.authorize_url = LOGIN_API_AUTH
        this.token_url = LOGIN_API_TOKEN
        break
      case 'NOTIFY':
        this.authorize_url = NOTIFY_API_AHTH
        this.token_url = NOTIFY_API_TOKEN
        break
      default:
        throw new Error(`Not support Line OAuth type: ${type}`)
        break
    }
  }
  set config (value) {
    this._config = value
  }
  get config () { return this._config }
  // Express.js middleware
  authorize({ clientId = '', clientSecret = '' }) {
    this._config = { clientId, clientSecret }
    return (req, res, next) => {
      const request_url = url.parse(req.originalUrl, true);
      const return_url = url.format({
        protocol: req.hostname === 'localhost' ? 'http' : 'https',
        host: req.headers.host,
        pathname: request_url.pathname,
      });
      const code = request_url.query && request_url.query.code;
      const state = request_url.query && request_url.query.state;
      const error = request_url.query && request_url.query.error;

      if (error) {
        const description = request_url.query.error_description || error;
        return next(new Error(description));
      }

      if (!code) {
        let reqContext = JSON.stringify({});
        if (req.context) {
          reqContext = JSON.stringify(req.context);
        }
        res.writeHead(302, {
          Location: this._make_acquire_url(reqContext, return_url)
        });
        res.end();
      }

      if (code) {
        const reqContext = JSON.parse(state);

        this._make_token_request(code, return_url)
          .then(response => {
            const accessToken = response.data && response.data.access_token;
            req['context'] = reqContext;
            if (response.data && response.data.id_token) {
              const idToken = jwtDecode(response.data.id_token);
              req['line-login-id-token'] = idToken;
            }

            if (this.type==='NOTIFY')
              req['line-notify-access-token'] = accessToken;

            if (this.type==='LOGIN')
              req['line-login-access-token'] = accessToken;

            next();
          })
          .catch(error => {
            return next(error);
          });
      }
    }
  }
  _make_acquire_url(reqContext, redirect_uri) {
    const scope = this.type === 'NOTIFY' ? 'notify' : 'openid profile nonce email'
    const params = new URLSearchParams({
      response_type: 'code',
      client_id: this._config.clientId,
      redirect_uri,
      scope,
      state: reqContext
    })
    return `${this.authorize_url}?${params.toString()}`
  }
  _make_token_request(code, redirect_uri) {
    const params = {
      client_id: this._config.clientId,
      client_secret: this._config.clientSecret,
      redirect_uri,
      code,
      grant_type: 'authorization_code'
    }
    return axios.post(this.token_url, querystring.stringify(params))
  }
}

module.exports = (type) => new LineOAuth(type)
module.exports.LineOAuth = LineOAuth
