const axios = require('axios')
const querystring = require('querystring')
const FormData = require('form-data')

const lineOAuth = require('./lineOAuth')('NOTIFY')
const endpoint = 'https://notify-api.line.me/api'

const clientId = process.env.NOTIFY_CLIENT_ID
const clientSecret = process.env.NOTIFY_CLIENT_SECRET

const middleware = lineOAuth.authorize({ clientId, clientSecret })

const sendMessage = ({ token, msgObj, imageFile }) => {
  if(!token) throw new Error('Notify api access_token is requested!')
  if(!msgObj || !msgObj.message)throw new Error('Send message content is requested!')

  const form = new FormData()
  Object.keys(msgObj).forEach((k)=>{
    form.append(k, msgObj[k])
  })
  if(imageFile){
    form.append('imageFile', imageFile.buffer, {
      filename: imageFile.originalname,
      contentType: imageFile.mimetype,
      knownLength: imageFile.size
    });
  }
  const headers = Object.assign({
    Accept: 'application/json',
    Authorization: 'Bearer ' + token
  }, form.getHeaders())
  return axios.post(`${endpoint}/notify`, form, { headers })
}

const getStatus = (token) => {
  return axios.get(`${endpoint}/status`, {headers: {Authorization: `Bearer ${token}`}})
}

const revokeToken = (token) => {
  return axios.post(`${endpoint}/revoke`, querystring.stringify({}), {headers: {Authorization: `Bearer ${token}`}})
}

module.exports = lineOAuth
module.exports.expressMiddleware = middleware
module.exports.sendMessage = sendMessage
module.exports.getStatus = getStatus
module.exports.revokeToken = revokeToken
