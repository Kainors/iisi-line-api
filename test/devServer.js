if (process.env.NODE_ENV === 'development') {
  require('dotenv').config()
}
const express = require('express')
const path = require('path')
const app = express()
const {
  bot,
  login
} = require('../index')
const port = process.env.PORT || 3000
app.use(express.static(path.join(__dirname,'./wwwroot')))

app.use('/line/login', login.expressMiddleware)
const notify = require('./notify')
app.use('/line/notify', notify)

app.listen(port, () => {
  console.log(`dev server start at port ${port}`)
})
