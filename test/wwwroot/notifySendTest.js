(function(){
  document.querySelector('#send').addEventListener('click', (e) => {
    const token = document.querySelector('#token').value
    const message = document.querySelector('#message').value
    const file = document.querySelector('#imageFile').files
    const form = new FormData()
    form.append('token', token)
    form.append('message', message)
    if(file[0]) form.append('imageFile', file[0])

    fetch('/line/notify/sendMessage', {
      method: 'POST',
      body: form
    })
  })
}())
