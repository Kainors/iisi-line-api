const express = require('express')
const router = express.Router()
const upload = require('multer')()
const {notify} = require('../index')

router.use('/auth', notify.expressMiddleware, (req, res) => {
  res.send(req['line-notify-access-token'])
})
router.get('/status', async (req, res) => {
  const {
    token
  } = req.query
  const result = await notify.getStatus(token)
  const data = await result.json()
  res.send(data)
})
router.get('/sendTest', (req, res) => {
  const {
    token
  } = req.query
  const msgObj = {
    message: 'message test'
  }
  notify.sendMessage({
    token,
    msgObj
  })
  res.send('ok')
})

router.post('/sendMessage', upload.single('imageFile'),async (req, res) => {
  const { token, message } = req.body
  let msgObj = req.body
  delete msgObj['token']
  const msgOpt = {token, msgObj}
  if(req.file) msgOpt['imageFile'] = req.file

  const response = await notify.sendMessage(msgOpt)
  if(response.statusText === 'OK'){
    const data = response.data
    console.log(data)
    res.send('OK')
  }
  else{
    // const result = await response.json()
    res.send('faild')
  }
})
router.get('/revoke', (req, res) => {
  const {
    token
  } = req.query
  notify.revokeToken(token)
  res.send('ok')
})

module.exports = router
