const login = require('./lib/login-api')
const bot = require('./lib/message-api')
const notify = require('./lib/notify-api')

module.exports = {
  login,
  bot,
  notify
}
